package View;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class TelaCadastroConsulta extends JInternalFrame {
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaCadastroConsulta frame = new TelaCadastroConsulta();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaCadastroConsulta() {
		setClosable(true);
		setBounds(0, 0, 450, 300);
		getContentPane().setLayout(null);
		
		JLabel lblMdico = new JLabel("Médico:");
		lblMdico.setBounds(12, 12, 70, 15);
		getContentPane().add(lblMdico);
		
		JLabel lblPaciente = new JLabel("Paciente:");
		lblPaciente.setBounds(12, 39, 70, 15);
		getContentPane().add(lblPaciente);
		
		JLabel lblNewLabel = new JLabel("Data:");
		lblNewLabel.setBounds(12, 68, 51, 15);
		getContentPane().add(lblNewLabel);
		
		JLabel lblHora = new JLabel("Hora:");
		lblHora.setBounds(12, 93, 70, 15);
		getContentPane().add(lblHora);
		
		JLabel lblDiagnstico = new JLabel("Diagnóstico:");
		lblDiagnstico.setBounds(12, 122, 99, 15);
		getContentPane().add(lblDiagnstico);
		
		textField = new JTextField();
		textField.setBounds(127, 10, 114, 19);
		getContentPane().add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(127, 37, 114, 19);
		getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(127, 66, 114, 19);
		getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(127, 91, 114, 19);
		getContentPane().add(textField_3);
		textField_3.setColumns(10);
		
		textField_4 = new JTextField();
		textField_4.setBounds(129, 120, 114, 19);
		getContentPane().add(textField_4);
		textField_4.setColumns(10);
		
		JButton btnNewButton = new JButton("Submeter");
		btnNewButton.setBounds(37, 196, 117, 25);
		getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Fechar");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			dispose();
			}
		});
		btnNewButton_1.setBounds(242, 196, 117, 25);
		getContentPane().add(btnNewButton_1);

	}

}
