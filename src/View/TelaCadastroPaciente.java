package View;

import java.awt.EventQueue;

import javax.swing.ButtonGroup;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JButton;

import Controlers.ControlerUsuario;
import Models.Paciente;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class TelaCadastroPaciente extends JInternalFrame {
	
	private JTextField txtNome;
	private JTextField txtIdade;
	private JTextField txtTelefone;
	private JTextField txtRg;
	private JTextField textField_5;
	private JTextField txtCpf;
	private JTextField txtSintomas;
	private Paciente paciente;
	
	static ControlerUsuario controladorPaciente;
	

	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaCadastroPaciente frame = new TelaCadastroPaciente();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaCadastroPaciente() {
		setClosable(true);
		setBounds(0, 0, 450, 300);
		getContentPane().setLayout(null);
		
		JLabel lblNome = new JLabel("Nome:");
		lblNome.setBounds(0, 7, 70, 15);
		getContentPane().add(lblNome);
		
		controladorPaciente = new ControlerUsuario();
		
		
		txtNome = new JTextField();
		txtNome.setBounds(78, 5, 114, 19);
		getContentPane().add(txtNome);
		txtNome.setColumns(10);
		
		JLabel lblIdade = new JLabel("Idade:");
		lblIdade.setBounds(0, 34, 70, 15);
		getContentPane().add(lblIdade);
		
		txtIdade = new JTextField();
		txtIdade.setBounds(78, 36, 114, 19);
		getContentPane().add(txtIdade);
		txtIdade.setColumns(10);
		
		JLabel lblTelefone = new JLabel("Telefone:");
		lblTelefone.setBounds(0, 68, 70, 15);
		getContentPane().add(lblTelefone);
		
		txtTelefone = new JTextField();
		txtTelefone.setBounds(78, 66, 114, 19);
		getContentPane().add(txtTelefone);
		txtTelefone.setColumns(10);
		
		JLabel lblRg = new JLabel("RG:");
		lblRg.setBounds(0, 99, 70, 15);
		getContentPane().add(lblRg);
		
		txtRg = new JTextField();
		txtRg.setBounds(78, 97, 114, 19);
		getContentPane().add(txtRg);
		txtRg.setColumns(10);
		
		JLabel lblCpf = new JLabel("CPF:");
		lblCpf.setBounds(0, 0, 440, 268);
		getContentPane().add(lblCpf);
		
		txtCpf = new JTextField();
		txtCpf.setBounds(78, 128, 114, 19);
		getContentPane().add(txtCpf);
		txtCpf.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Sintomas:");
		lblNewLabel.setBounds(0, 157, 81, 15);
		getContentPane().add(lblNewLabel);
		
		txtSintomas = new JTextField();
		txtSintomas.setBounds(78, 159, 114, 19);
		getContentPane().add(txtSintomas);
		txtSintomas.setColumns(10);
		
		final JRadioButton rdbtnPediatria = new JRadioButton("Pediatria");
		rdbtnPediatria.setBounds(8, 205, 103, 23);
		getContentPane().add(rdbtnPediatria);
		
		final JRadioButton rdbtnClinicaMedica = new JRadioButton("Clinica Médica");
		rdbtnClinicaMedica.setBounds(140, 205, 125, 23);
		getContentPane().add(rdbtnClinicaMedica);
		
		ButtonGroup group = new ButtonGroup();
		group.add(rdbtnPediatria);
		group.add(rdbtnClinicaMedica);
		
		JButton btnSubmeter = new JButton("Submeter");
		btnSubmeter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String pediatria;
				String clinicaMedica;
				if(rdbtnPediatria.isSelected())
					pediatria = "Pediatria";
				
				else if (rdbtnClinicaMedica.isSelected())
					clinicaMedica = "Clinica Medica";
				
				String nome = txtNome.getText();
				String idade = txtIdade.getText();
				String telefone = txtTelefone.getText();
				String cpf = txtCpf.getText();
				String rg = txtRg.getText();
				String sintomas = txtSintomas.getText();
				
				
				paciente = new Paciente(nome, sintomas);
				
				paciente.setIdade(idade);
				paciente.setTelefone(telefone);
				paciente.setCpf(cpf);
				paciente.setRg(rg);
				paciente.setSintomas(sintomas);
				
				
				
				
				controladorPaciente.adicionarUmPaciente(paciente);
				JOptionPane.showMessageDialog(null, "Paciente cadastrado com sucesso!");
				
				
				
			
				
			}
		});
		btnSubmeter.setBounds(283, 29, 117, 25);
		getContentPane().add(btnSubmeter);
		
		JButton btnCancelar = new JButton("Fechar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancelar.setBounds(283, 81, 117, 25);
		getContentPane().add(btnCancelar);

	}
}
