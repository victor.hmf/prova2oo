package View;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;

public class TelaBuscarMedico extends JInternalFrame {
	private JTextField txtNome;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaBuscarMedico frame = new TelaBuscarMedico();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaBuscarMedico() {
		setClosable(true);
		setBounds(0, 0, 450, 300);
		getContentPane().setLayout(null);
		
		JLabel lblNome = new JLabel("Nome:");
		lblNome.setBounds(58, 35, 70, 15);
		getContentPane().add(lblNome);
		
		txtNome = new JTextField();
		txtNome.setBounds(217, 33, 114, 19);
		getContentPane().add(txtNome);
		txtNome.setColumns(10);
		
		
		
		JButton btnFechar = new JButton("Fechar");
		btnFechar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnFechar.setBounds(252, 207, 117, 25);
		getContentPane().add(btnFechar);
		
		final JTextArea txtMostra = new JTextArea();
		txtMostra.setBounds(68, 62, 286, 127);
		getContentPane().add(txtMostra);
		
		JButton btnSubmeter = new JButton("Submeter");
		btnSubmeter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				try{
					txtMostra.setText("Nome: " + TelaCadastroMedico.controladorMedico.buscarMedico(txtNome.getText()).getNome());
					txtMostra.append("\nIdade: " + TelaCadastroMedico.controladorMedico.buscarMedico(txtNome.getText()).getIdade());
					txtMostra.append("\nTelefone: " + TelaCadastroMedico.controladorMedico.buscarMedico(txtNome.getText()).getTelefone());
					txtMostra.append("\nCPF: " + TelaCadastroMedico.controladorMedico.buscarMedico(txtNome.getText()).getCpf());
					txtMostra.append("\nRG: " + TelaCadastroMedico.controladorMedico.buscarMedico(txtNome.getText()).getRg());
			
					
					}catch(NullPointerException a){
							txtMostra.setText("Medico não encontrado");
					
					}
			}
		});
		btnSubmeter.setBounds(72, 207, 117, 25);
		getContentPane().add(btnSubmeter);

	}

}
