package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JDesktopPane;

public class TelaPrincipal extends JFrame {

	private JPanel contentPane;
	private JMenuItem mntmPaciente;
	private TelaCadastroPaciente telaCadastroPaciente;
	private TelaCadastroMedico telaCadastroMedico;
	private JMenuItem mntmMdico;
	private TelaCadastroConsulta telaCadastroConsulta;
	private JMenuItem mntmCo;
	private JMenuItem mntmMdico_1;
	private TelaBuscarPaciente telaBuscarPaciente;
	private TelaBuscarMedico telaBuscarMedico;
	private TelaBuscarConsulta telaBuscarConsulta;
	private TelaRemoverPaciente telaRemoverPaciente;
	private TelaRemoverMedico telaRemoverMedico;
	private TelaRemoverConsulta telaRemoverConsulta;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaPrincipal frame = new TelaPrincipal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaPrincipal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 515, 360);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnMenu = new JMenu("Menu");
		menuBar.add(mnMenu);
		
		JMenu mnCadastro = new JMenu("Cadastro");
		mnMenu.add(mnCadastro);
		
		mntmPaciente = new JMenuItem("Paciente");
		
		mnCadastro.add(mntmPaciente);
		
		mntmMdico = new JMenuItem("Médico");
		
		mnCadastro.add(mntmMdico);
		
		mntmCo = new JMenuItem("Consulta");
		
		mnCadastro.add(mntmCo);
		
		JMenu mnBuscar = new JMenu("Buscar");
		mnMenu.add(mnBuscar);
		
		JMenuItem mntmPaciente_1 = new JMenuItem("Paciente");
		
		
		mnBuscar.add(mntmPaciente_1);
		
		mntmMdico_1 = new JMenuItem("Médico");
		
		mnBuscar.add(mntmMdico_1);
		
		JMenuItem mntmConsulta = new JMenuItem("Consulta");
		
		mnBuscar.add(mntmConsulta);
		
		JMenu mnRemover = new JMenu("Remover");
		mnMenu.add(mnRemover);
		
		JMenuItem mntmPaciente_2 = new JMenuItem("Paciente");
		
		mnRemover.add(mntmPaciente_2);
		
		JMenuItem mntmMdico_2 = new JMenuItem("Médico");
		
		mnRemover.add(mntmMdico_2);
		
		JMenuItem mntmConsulta_1 = new JMenuItem("Consulta");
		
		mnRemover.add(mntmConsulta_1);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		final JDesktopPane desktopPane = new JDesktopPane();
		desktopPane.setBounds(6, 0, 474, 335);
		contentPane.add(desktopPane);
		
		mntmPaciente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(telaCadastroPaciente == null || telaCadastroPaciente.isClosed()){
					telaCadastroPaciente = new TelaCadastroPaciente();
					desktopPane.add(telaCadastroPaciente);
					telaCadastroPaciente.show();
					
				}
				
			}
		});
		
		mntmMdico.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(telaCadastroMedico == null || telaCadastroMedico.isClosed()){
					telaCadastroMedico = new TelaCadastroMedico();
					desktopPane.add(telaCadastroMedico);
					telaCadastroMedico.show();
				}
			}
		});
		
		mntmCo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(telaCadastroConsulta == null || telaCadastroConsulta.isClosed()){
					telaCadastroConsulta = new TelaCadastroConsulta();
					desktopPane.add(telaCadastroConsulta);
					telaCadastroConsulta.show();
				
				
				}
			}
		});
		
		mntmPaciente_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(telaBuscarPaciente == null || telaBuscarPaciente.isClosed()){
					telaBuscarPaciente = new TelaBuscarPaciente();
					desktopPane.add(telaBuscarPaciente);
					telaBuscarPaciente.show();
				}
			}
		});
		
		mntmMdico_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(telaBuscarMedico == null || telaBuscarMedico.isClosed()){
					telaBuscarMedico = new TelaBuscarMedico();
					desktopPane.add(telaBuscarMedico);
					telaBuscarMedico.show();
				}
			}
		});
		
		mntmConsulta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(telaBuscarConsulta == null || telaBuscarConsulta.isClosed()){
					telaBuscarConsulta = new TelaBuscarConsulta();
					desktopPane.add(telaBuscarConsulta);
					telaBuscarConsulta.show();
				}
			}
		});
		
		mntmPaciente_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(telaRemoverPaciente == null || telaRemoverPaciente.isClosed()){
					telaRemoverPaciente = new TelaRemoverPaciente();
					desktopPane.add(telaRemoverPaciente);
					telaRemoverPaciente.show();
				}		
			}
		});
		
		mntmMdico_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(telaRemoverMedico == null || telaRemoverMedico.isClosed()){
					telaRemoverMedico = new TelaRemoverMedico();
					desktopPane.add(telaRemoverMedico);
					telaRemoverMedico.show();
				}
			}
		});
		
		mntmConsulta_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(telaRemoverConsulta == null || telaRemoverConsulta.isClosed()){
					telaRemoverConsulta = new TelaRemoverConsulta();
					desktopPane.add(telaRemoverConsulta);
					telaRemoverConsulta.show();
				}
			}
		});
		
		
	}
}
