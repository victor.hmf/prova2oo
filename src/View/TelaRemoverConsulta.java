package View;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JTextPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class TelaRemoverConsulta extends JInternalFrame {
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaRemoverConsulta frame = new TelaRemoverConsulta();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaRemoverConsulta() {
		setClosable(true);
		setBounds(0, 0, 450, 300);
		getContentPane().setLayout(null);
		
		JLabel lblNome = new JLabel("Nome do Paciente:");
		lblNome.setBounds(60, 45, 156, 15);
		getContentPane().add(lblNome);
		
		textField = new JTextField();
		textField.setBounds(234, 43, 114, 19);
		getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btnSubmeter = new JButton("Submeter");
		btnSubmeter.setBounds(72, 183, 117, 25);
		getContentPane().add(btnSubmeter);
		
		JButton btnFechar = new JButton("Fechar");
		btnFechar.addActionListener(new ActionListener() {
			
		public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnFechar.setBounds(279, 183, 117, 25);
		getContentPane().add(btnFechar);

	}
}
