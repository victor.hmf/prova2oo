package View;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;

public class TelaBuscarPaciente extends JInternalFrame {
	private JTextField txtNome;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaBuscarPaciente frame = new TelaBuscarPaciente();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaBuscarPaciente() {
		setClosable(true);
		setBounds(0, 0, 450, 300);
		getContentPane().setLayout(null);
		
		JLabel lblNome = new JLabel("Nome:");
		lblNome.setBounds(64, 42, 70, 15);
		getContentPane().add(lblNome);
		
		txtNome = new JTextField();
		txtNome.setBounds(217, 40, 114, 19);
		getContentPane().add(txtNome);
		txtNome.setColumns(10);
		
	
		
		JButton btnFechar = new JButton("Fechar");
		btnFechar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					dispose();
			}
		});
		btnFechar.setBounds(251, 194, 117, 25);
		getContentPane().add(btnFechar);
		
		final JTextArea txtMostra = new JTextArea();
		txtMostra.setBounds(42, 69, 333, 111);
		getContentPane().add(txtMostra);
		
		

		JButton btnSubmeter = new JButton("Submeter");
		btnSubmeter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
				txtMostra.setText("Nome: " + TelaCadastroPaciente.controladorPaciente.buscarPaciente(txtNome.getText()).getNome());
				txtMostra.append("\nIdade: " + TelaCadastroPaciente.controladorPaciente.buscarPaciente(txtNome.getText()).getIdade());
				txtMostra.append("\nTelefone: " + TelaCadastroPaciente.controladorPaciente.buscarPaciente(txtNome.getText()).getTelefone());
				txtMostra.append("\nCPF: " + TelaCadastroPaciente.controladorPaciente.buscarPaciente(txtNome.getText()).getCpf());
				txtMostra.append("\nRG: " + TelaCadastroPaciente.controladorPaciente.buscarPaciente(txtNome.getText()).getRg());
				txtMostra.append("\nSintomas: " + TelaCadastroPaciente.controladorPaciente.buscarPaciente(txtNome.getText()).getSintomas());
				
				}catch(NullPointerException a){
						txtMostra.setText("Paciente não encontrado");
				
				}
			}
			
		});

		btnSubmeter.setBounds(60, 194, 117, 25);
		getContentPane().add(btnSubmeter);
	}
}

