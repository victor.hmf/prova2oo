package View;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

import Models.Paciente;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class TelaRemoverPaciente extends JInternalFrame {
	private JTextField txtNome;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaRemoverPaciente frame = new TelaRemoverPaciente();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaRemoverPaciente() {
		setClosable(true);
		setBounds(0, 0, 450, 300);
		getContentPane().setLayout(null);
		
		JLabel lblNome = new JLabel("Nome:");
		lblNome.setBounds(61, 51, 70, 15);
		getContentPane().add(lblNome);
		
		txtNome = new JTextField();
		txtNome.setBounds(227, 49, 114, 19);
		getContentPane().add(txtNome);
		txtNome.setColumns(10);
		
		JButton btnSubmeter = new JButton("Submeter");
		btnSubmeter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try{
				 	
						String nome = txtNome.getText();
				 	
						Paciente umPaciente = TelaCadastroPaciente.controladorPaciente.buscarPaciente(nome);
				
						TelaCadastroPaciente.controladorPaciente.removerPaciente(umPaciente);
				 	
						JOptionPane.showMessageDialog(null, "Paciente excluido com sucesso!");
				 
						}catch(NullPointerException a){
				 	
						JOptionPane.showMessageDialog(null, "Paciente não existente");
				 
			}
				
			}
		});
		btnSubmeter.setBounds(61, 188, 117, 25);
		getContentPane().add(btnSubmeter);
		
		JButton btnFechar = new JButton("Fechar");
		btnFechar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnFechar.setBounds(258, 188, 117, 25);
		getContentPane().add(btnFechar);

	}

}
