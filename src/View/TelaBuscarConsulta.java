package View;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class TelaBuscarConsulta extends JInternalFrame {
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaBuscarConsulta frame = new TelaBuscarConsulta();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaBuscarConsulta() {
		setClosable(true);
		setBounds(0, 0, 450, 300);
		getContentPane().setLayout(null);
		
		JLabel lblNomePaciente = new JLabel("Nome Paciente:");
		lblNomePaciente.setBounds(12, 27, 120, 15);
		getContentPane().add(lblNomePaciente);
		
		textField = new JTextField();
		textField.setBounds(179, 25, 114, 19);
		getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btnSubmeter = new JButton("Submeter");
		btnSubmeter.setBounds(54, 199, 117, 25);
		getContentPane().add(btnSubmeter);
		
		JButton btnFechar = new JButton("Fechar");
		btnFechar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnFechar.setBounds(261, 199, 117, 25);
		getContentPane().add(btnFechar);
		
		textField_1 = new JTextField();
		textField_1.setBounds(54, 54, 327, 130);
		getContentPane().add(textField_1);
		textField_1.setColumns(10);

	}

}
