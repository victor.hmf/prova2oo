package View;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;

import java.awt.BorderLayout;

import javax.swing.ButtonGroup;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JButton;

import Controlers.ControlerUsuario;
import Models.Medico;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class TelaCadastroMedico extends JInternalFrame {
	private JTextField txtNome;
	private JTextField txtIdade;
	private JTextField txtTelefone;
	private JTextField txtRg;
	private JTextField txtCpf;
	private JTextField txtRegistroMedico;
	private JLabel lblRegistro;
	private JLabel lblAreaAtuao;
	private JTextField txtAreaAtuacao;
	private JLabel lblTempoDeServio;
	private JTextField txtTempoDeServico;
	private JLabel lblCirurgio;
	 JButton btnSubmeter;
	 JButton btnCancelar;
	private Medico medico;
	
	static ControlerUsuario controladorMedico;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaCadastroMedico frame = new TelaCadastroMedico();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaCadastroMedico() {
		setClosable(true);
		setBounds(0, 0, 450, 300);
		getContentPane().setLayout(null);
		
		JLabel lblNome = new JLabel("Nome:");
		lblNome.setBounds(0, 7, 70, 15);
		getContentPane().add(lblNome);
		
		controladorMedico = new ControlerUsuario();
		
		txtNome = new JTextField();
		txtNome.setBounds(78, 5, 114, 19);
		getContentPane().add(txtNome);
		txtNome.setColumns(10);
		
		JLabel lblIdade = new JLabel("Idade:");
		lblIdade.setBounds(0, 34, 70, 15);
		getContentPane().add(lblIdade);
		
		txtIdade = new JTextField();
		txtIdade.setBounds(78, 36, 114, 19);
		getContentPane().add(txtIdade);
		txtIdade.setColumns(10);
		
		JLabel lblTelefone = new JLabel("Telefone:");
		lblTelefone.setBounds(0, 68, 70, 15);
		getContentPane().add(lblTelefone);
		
		txtTelefone = new JTextField();
		txtTelefone.setBounds(78, 66, 114, 19);
		getContentPane().add(txtTelefone);
		txtTelefone.setColumns(10);
		
		JLabel lblRg = new JLabel("RG:");
		lblRg.setBounds(0, 99, 70, 15);
		getContentPane().add(lblRg);
		
		txtRg = new JTextField();
		txtRg.setBounds(78, 97, 114, 19);
		getContentPane().add(txtRg);
		txtRg.setColumns(10);
		
		JLabel lblCpf = new JLabel("CPF:");
		lblCpf.setBounds(0, 130, 70, 15);
		getContentPane().add(lblCpf);
		
		txtCpf = new JTextField();
		txtCpf.setBounds(78, 128, 114, 19);
		getContentPane().add(txtCpf);
		txtCpf.setColumns(10);
		
		txtRegistroMedico = new JTextField();
		txtRegistroMedico.setBounds(78, 159, 114, 19);
		getContentPane().add(txtRegistroMedico);
		txtRegistroMedico.setColumns(10);
		
		lblRegistro = new JLabel("Registro:");
		lblRegistro.setBounds(0, 157, 70, 15);
		getContentPane().add(lblRegistro);
		
		lblAreaAtuao = new JLabel("Area Atuação:");
		lblAreaAtuao.setBounds(210, 7, 108, 15);
		getContentPane().add(lblAreaAtuao);
		
		txtAreaAtuacao = new JTextField();
		txtAreaAtuacao.setBounds(314, 5, 114, 19);
		getContentPane().add(txtAreaAtuacao);
		txtAreaAtuacao.setColumns(10);
		
		lblTempoDeServio = new JLabel("Tempo de Serviço:");
		lblTempoDeServio.setBounds(210, 34, 138, 15);
		getContentPane().add(lblTempoDeServio);
		
		txtTempoDeServico = new JTextField();
		txtTempoDeServico.setBounds(352, 32, 76, 19);
		getContentPane().add(txtTempoDeServico);
		txtTempoDeServico.setColumns(10);
		
		lblCirurgio = new JLabel("Cirurgião:");
		lblCirurgio.setBounds(278, 99, 70, 15);
		getContentPane().add(lblCirurgio);
		
		final JRadioButton rdbtnSim = new JRadioButton("Sim");
		rdbtnSim.setBounds(226, 126, 55, 23);
		getContentPane().add(rdbtnSim);
		
		
		
		final JRadioButton rdbtnNo = new JRadioButton("Não");
		rdbtnNo.setBounds(314, 126, 70, 23);
		getContentPane().add(rdbtnNo);
		
		ButtonGroup group = new ButtonGroup();
		group.add(rdbtnNo);
		group.add(rdbtnSim);
		
		btnSubmeter = new JButton("Submeter");
		btnSubmeter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String No;
				String Sim;
				if(rdbtnNo.isSelected())
					No = "Não";
				
				else if (rdbtnSim.isSelected())
					Sim = "Sim";
				
				String nome = txtNome.getText();
				
			
				String idade = txtIdade.getText();
				String telefone = txtTelefone.getText();
				String cpf = txtCpf.getText();
				String rg = txtRg.getText();
				String registroMedico = txtRegistroMedico.getText();
				String areaAtuacao = txtAreaAtuacao.getText();
				String tempoServico = txtTempoDeServico.getText();
				
				medico = new Medico(nome, registroMedico);
				
				medico.setIdade(idade);
				medico.setTelefone(telefone);
				medico.setCpf(cpf);
				medico.setRg(rg);
				medico.setAreaAtuacao(areaAtuacao);
				medico.setTempoDeServico(tempoServico);
				
				controladorMedico.adicionarUmMedico(medico);
				JOptionPane.showMessageDialog(null, "Medico cadastrado com sucesso!");
				
				
				
				
				
				
			}
		});
		btnSubmeter.setBounds(67, 210, 117, 25);
		getContentPane().add(btnSubmeter);
		
		btnCancelar = new JButton("Fechar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancelar.setBounds(248, 210, 117, 25);
		getContentPane().add(btnCancelar);

	}
}
