package Controlers;

import java.util.ArrayList;

import Models.ConsultaMedica;

public class ControlerConsultaMedica {
	
	ArrayList<ConsultaMedica> listaConsulta;
	
	public ControlerConsultaMedica(){
			listaConsulta = new ArrayList<ConsultaMedica>();
	}
	public void adicionarConsulta(ConsultaMedica umaConsulta){
		listaConsulta.add(umaConsulta);
	}	
	public ConsultaMedica buscar(String umPaciente){
		for (ConsultaMedica umaConsulta: listaConsulta){
			if(umaConsulta.getUmPaciente().getNome().equalsIgnoreCase(umPaciente)){
				return umaConsulta;
			}
		}
		return null;
	}
	public void remover(ConsultaMedica umaConsulta){
		listaConsulta.remove(umaConsulta);
	}
}