package Models;

public class Paciente extends Usuario{
	
	private String sintomas;
	private String clinicaMedica, pediatria;
	
	
	public Paciente(String nome, String sintomas) {
		super(nome);
		this.sintomas = sintomas;
	}
	
	public String getSintomas() {
		return sintomas;
	}
	public void setSintomas(String sintomas) {
		this.sintomas = sintomas;
	}
	public String isClinicaMedica() {
		return clinicaMedica;
	}
	public void setClinicaMedica(String clinicaMedica) {
		this.clinicaMedica = clinicaMedica;
	}
	public String isPediatria() {
		return pediatria;
	}
	public void setPediatria(String pediatria) {
		this.pediatria = pediatria;
	}
	
	

}